#
# Installs compat libraries when needed.  Debian doesn't need the help,
# since package dependencies should take care of this and we don't run
# as much unpackaged software on Debian.

class compatlibs {
    case $operatingsystem {
        redhat: {
            # we have to do this to get around lsbdistrelease differences in 
            # RHEL5 to 5.1 (etc).
            case $lsbdistrelease {
                3: {
                    package { "compat-db":      ensure => present; }
                }
                4: {
                    package {
                        "compat-db":            ensure => present;
                        "compat-libstdc++-33":  ensure => present;
                        "compat-libstdc++-296": ensure => present;
                    }
                }
                default: {
                    package {
                        "compat-db":            ensure => present;
                        "compat-libstdc++-33":  ensure => present;
                        "compat-libstdc++-296": ensure => present;
                    }
                }
            }
        }
    }
}